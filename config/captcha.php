<?php
/*
 * Secret key and Site key get on https://www.google.com/recaptcha
 * */
return [
    'secret' => '6Lc4xvAZAAAAAHmSjKuhbRgLYL6O0LHGDt3gd2xS',
    'sitekey' => '6Lc4xvAZAAAAAG7VfCEkk6_RoNuzAA-7PM9kjPBQ',
    /**
     * @var string|null Default ``null``.
     * Custom with function name (example customRequestCaptcha) or class@method (example \App\CustomRequestCaptcha@custom).
     * Function must be return instance, read more in repo ``https://github.com/thinhbuzz/laravel-google-captcha-examples``
     */
    'request_method' => null,
    'options' => [
        'multiple' => false,
        'lang' => 'nl',
    ],
    'attributes' => [
        'theme' => 'light'
    ],
];