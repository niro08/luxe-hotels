<?php

//namespace database\seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SettingsTableSeeder extends Seeder
{
    /**
     * The settings to add.
     */
    protected $settings = [
//                [
//            'key'         => 'contact_us_text',
//            'name'        => 'Contact Us Content',
//            'description' => 'Contact Us Page Text Content',
//            'value'       => '<p><strong>Heb je een vraag?</strong></p>
//<p>Stel je vraag gemakkelijk via het contactformulier en wij proberen je vraag binnen 24 uur te beantwoorden.</p>',
//            'field'       => '{"name":"value","label":"Value","type":"wysiwyg"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'homepage_meta_title',
//            'name'        => 'Homepage Meta Title',
//            'description' => 'Homepage Meta Title',
//            'value'       => 'Luxe-Hotels',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'homepage_meta_description',
//            'name'        => 'Homepage Meta Description',
//            'description' => 'Homepage Meta Description',
//            'value'       => 'Luxe-Hotels',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'homepage_meta_keywords',
//            'name'        => 'Homepage Meta Keywords',
//            'description' => 'Homepage Meta Keywords',
//            'value'       => 'hotel,luxe',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'allhotels_meta_title',
//            'name'        => 'Allhotels Meta Title',
//            'description' => 'Allhotels Meta Title',
//            'value'       => 'All Hotels in Nederland',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'allhotels_meta_description',
//            'name'        => 'Allhotels Meta Description',
//            'description' => 'Allhotels Meta Description',
//            'value'       => 'Luxe-Hotels',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'allhotels_meta_keywords',
//            'name'        => 'Allhotels Meta Keywords',
//            'description' => 'Allhotels Meta Keywords',
//            'value'       => 'hotel,luxe',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'deals_meta_title',
//            'name'        => 'Deals Meta Title',
//            'description' => 'Deals Page Meta Title',
//            'value'       => 'Deals Hotels in Nederland',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'deals_meta_description',
//            'name'        => 'Deals Meta Description',
//            'description' => 'Deals Page Meta Description',
//            'value'       => 'Luxe-Hotels',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'deals_meta_keywords',
//            'name'        => 'Deals Meta Keywords',
//            'description' => 'Deals Page Meta Keywords',
//            'value'       => 'hotel,luxe',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'inspiration_meta_title',
//            'name'        => 'Inspiration Meta Title',
//            'description' => 'Inspiration Page Meta Title',
//            'value'       => 'Inspiratie',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'inspiration_meta_description',
//            'name'        => 'Inspiration Meta Description',
//            'description' => 'Inspiration Page Meta Description',
//            'value'       => 'Luxe-Hotels Inspratie',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
//        [
//            'key'         => 'inspiration_meta_keywords',
//            'name'        => 'Inspiration Meta Keywords',
//            'description' => 'Inspiration Page Meta Keywords',
//            'value'       => 'hotel,luxe,inspiratie',
//            'field'       => '{"name":"value","label":"Value","type":"text"}',
//            'active'      => 1,
//        ],
        [
            'key'         => 'homepage_bottom_content',
            'name'        => 'Homepage bottom content',
            'description' => 'Homepage bottom content',
            'value'       => '',
            'field'       => '{"name":"value","label":"Value","type":"wysiwyg"}',
            'active'      => 1,
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->settings as $index => $setting) {
            $result = DB::table('settings')->insert($setting);

            if (!$result) {
                $this->command->info("Insert failed at record $index.");

                return;
            }
        }

        $this->command->info('Inserted '.count($this->settings).' records.');
    }
}
