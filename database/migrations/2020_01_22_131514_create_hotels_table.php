<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotels', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->unsignedBigInteger('city_id');
//            $table->unsignedBigInteger('province_id');
            $table->text('content');
            $table->string('booking_link')->nullable();
            $table->float('price')->unsigned();
            //numbers,address,phone
            $table->text('head_info')->nullable();
            //tabs
            $table->text('services_info')->nullable();
            $table->text('general_info')->nullable();
            $table->boolean('is_deal')->default(0);
            $table->text('meta')->nullable();
            $table->timestamps();

            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotels', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
        });
        Schema::dropIfExists('hotels');
    }
}
