<?php

namespace App\Http\Controllers\Api;


use App\Helpers\SearchHelper;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function index()
    {
        $result = SearchHelper::getResult();
        return response()->json(['results' => $result]);
    }
}
