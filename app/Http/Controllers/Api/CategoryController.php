<?php

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Helpers\CategoryHelper;
use App\Models\Hotel;
use App\Http\Resources\HotelResource;

class CategoryController extends Controller
{
    protected $_categoryHelper;

    public function __construct()
    {
        $this->_categoryHelper = CategoryHelper::getlnstance();
    }

    public function filter(Request $request)
    {

        $currentIds = $request->currentIds;
        $filter = $request->filter;
        $activeFilters = $request->activeFilters;
        $content = null;

        //Current Hotels
        $currentHotels = Hotel::whereIn('id', $currentIds)->get();
        $currentHotelsIds = $currentHotels->pluck('id')->toArray();

        //get model by filter value
        $model = $this->_categoryHelper->getModel($filter['name'], 'id', $filter['value']);

        //model hotels
        $modelHotels = $model->hotels;
        $modelHotelsIds = $modelHotels->pluck('id')->toArray();

        //if last and current filter names is same take new collection and content from model,
        // otherwise filter current collection
        $filteredHotelsIds = array_intersect($currentHotelsIds, $modelHotelsIds);
        if (!empty($filteredHotelsIds)) {
            $hotels = Hotel::whereIn('id', $filteredHotelsIds)->get();
        } else {
            $hotels = $model->hotels;
            $content = $model->content;
        }

        $this->_categoryHelper->setProperty('currentFilters', $activeFilters);
        $title = $this->_categoryHelper->getTitle();

        //available filter options for filtered hotels collection
        foreach ($activeFilters as $key => $param) {
            $filters[$key] = $hotels->getFilterOptions($key);
        }

        //Prepare Collection for view
        $hotels = HotelResource::collection($hotels)->resolve();
        return response()->json(['title' => $title, 'content' => $content, 'hotels' => $hotels, 'filters' => $filters]);
    }
}