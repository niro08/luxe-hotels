<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Models\Review;
use App\Http\Controllers\Controller;
use App\Http\Resources\ReviewResource;

class ReviewController extends Controller
{
    public function store(Request $request)
    {
        $review = $this->validate($request, [
            'name' => 'required|min:2|max:50',
            'email' => 'required|email',
            'title' => 'required|min:3|max:50',
            'body' => 'required|min:3',
            'score' => 'required',
            'hotel_id' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        $review = Review::create($review);

        return new ReviewResource($review);
    }
}
