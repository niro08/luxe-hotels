<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Review;
use App\Http\Resources\ReviewResource;
use Illuminate\Http\Request;

class HotelController extends Controller
{
    public function show($slug){
        $hotel = Hotel::where('slug',$slug)->firstOrFail();
        $gallery = $hotel->getImagePathArray();
        $reviews = $hotel->approvedReviews()->get();
        $reviews = ReviewResource::collection($reviews);

        return view('hotel',compact('hotel','gallery','reviews'));
    }

    public function book($slug)
    {
        $hotel = Hotel::where('slug',$slug)->firstOrFail();
        return $hotel->booking_link ? redirect()->to($hotel->booking_link) : back();
    }
}
