<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Setting;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function index(){
        $articles = Article::simplePaginate(9)->sortByDesc('created_at');
        $meta = [
            'title' => Setting::get('blog_meta_title'),
            'keywords' => Setting::get('blog_meta_keywords'),
            'description' => Setting::get('blog_meta_description'),
        ];
        return view('blog/list',compact('articles','meta'));
    }
    public function show(Article $article){

        $gallery = $article->getImagePathArray();
        return view('blog/view',compact('article','gallery'));
    }
}