<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Block;
use App\Models\Setting;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blocks = Block::where('is_active',1)
            ->orderBy('sort_order')
            ->get();
        $meta = [
            'title' => Setting::get('homepage_meta_title'),
            'description' => Setting::get('homepage_meta_description'),
            'keywords' => Setting::get('homepage_meta_description')
        ];
        $content = Setting::get('homepage_bottom_content');
        return view('home',compact('blocks','meta','content'));
    }
}
