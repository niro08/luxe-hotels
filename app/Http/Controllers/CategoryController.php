<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Category;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Helpers\CategoryHelper;
use App\Http\Resources\HotelResource;

class CategoryController extends Controller
{

    protected $_categoryHelper;
    private $_filterAttributes = ['city', 'province', 'category'];


    public function __construct()
    {
        $this->_categoryHelper = CategoryHelper::getlnstance();
    }

    //refactored, but still need to be refatored =)
    public function show(Request $request)
    {
        $content = '';

        //Prepare collection for ShowAll and Deals page
        if (strpos($request->getPathInfo(), 'deals')) {
            $hotels = Hotel::where('is_deal', 1)->get();
        } else {
            $hotels = Hotel::all();
        }

        //prepare model based on request
        $defaultFilters = array_combine($this->_filterAttributes, [$request->city, $request->province, $request->category]);
        $hasCollection = false;
        $model = null;
        foreach ($defaultFilters as $key => $param) {
            if ($param) {
                $model = $this->_categoryHelper->getModel($key, 'slug', $param);
                //first param is primary model
                if (!$hasCollection) {
                    $content = $model->content;
                    $hotels = $model->hotels;
                    $hasCollection = true;
                }
                //selected filter options
                $defaultFilters[$key] = $model->id;
            }
        }
        //available filter options for hotels collection
        foreach ($defaultFilters as $key => $param) {
            if (isset($model)) {
                $filters[$key] = $model->hotels->getFilterOptions($key);
            } else {
                $filters[$key] = $hotels->getFilterOptions($key);
            }
        }

        //Prepare Title, Current Filters and Meta
        $this->_categoryHelper->setProperty('currentFilters', $defaultFilters);
        $title = $this->_categoryHelper->getTitle();
        $meta = $this->_categoryHelper->getMeta();

        //Scheise
        $defaultFilters = $this->_categoryHelper->getCurrentFiltersIds();

        //Prepare Collection for view
        $hotels = HotelResource::collection($hotels)->resolve();

        return view('category', compact(['title', 'content', 'hotels', 'filters', 'defaultFilters', 'meta']));
    }

    public function inspiration()
    {
        $categories = Category::has('hotels')->get();
        $meta = [
            'title' => Setting::get('inspiration_meta_title'),
            'description' => Setting::get('inspiration_meta_description'),
            'keywords' => Setting::get('inspiration_meta_keywords')
        ];
        return view('inspiration', compact('categories', 'meta'));
    }
}