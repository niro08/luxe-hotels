<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Models\Contact;

class ContactCrudController extends CrudController {

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
//    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation { show as traitShow; }
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\Contact");
        $this->crud->setRoute(config('backpack.base.route_prefix') . "/contact");
        $this->crud->setEntityNameStrings('contact', 'contacts');

    }

    public function show($id){
        $contact = Contact::find($id);
        $contact->is_viewed = true;
        $contact->save();

        $content = $this->traitShow($id);

        return $content;
    }

    protected function setupListOperation()
    {
        $this->crud->addColumn(['name' => 'name', 'type' => 'text', 'label' => 'Name']);

        $this->crud->addColumn(['name' => 'email', 'type' => 'text', 'label' => 'Email']);

        $this->crud->addColumn(['name' => 'phone_number', 'type' => 'hidden', 'label' => 'Phone']);

        $this->crud->addColumn(['name' => 'message', 'type' => 'text', 'label' => 'Message']);

        $this->crud->addColumn(['name' => 'created_at', 'type' => 'datetime', 'label' => 'Created At']);

        $this->crud->addColumn(['name' => 'is_viewed', 'type' => 'boolean', 'label' => 'Viewed']);

        $this->crud->orderBy('id','desc');

    }

//    protected function setupCreateOperation()
//    {
//        $this->crud->setValidation(BlockRequest::class);
//
//        $this->crud->addField([
//            'name' => 'title',
//            'label' => 'Title',
//            'tab' => 'General info',
//        ]);
//
//        $this->crud->addField([
//            'name' => 'subtitle',
//            'label' => 'Subtitle',
//            'tab' => 'General info',
//        ]);
//
//        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
//            'label'     => "Hotels",
//            'type'      => 'select2_multiple',
//            'name'      => 'hotels', // the method that defines the relationship in your Model
//            'entity'    => 'hotels', // the method that defines the relationship in your Model
//            'attribute' => 'title', // foreign key attribute that is shown to user
//            'model'     => "App\Models\Hotel", // foreign key model
//            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
//            'options'   => (function ($query) {
//                return $query->orderBy('title', 'ASC')->get();
//            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
//            'tab' => 'General info',
//        ]);
//
//        $this->crud->addField([   // URL
//            'name' => 'url',
//            'label' => 'View All Link',
//            'type' => 'text',
//            'hint' => 'e.g. "nederland/inspiratie/city-hotels"',
//            'tab' => 'General info',
//        ]);
//
//        $this->crud->addField([
//            'name' => 'sort_order',
//            'type' => 'number',
//            'label' => 'Sort Order',
//            'tab' => 'General info'
//        ]);
//
//        $this->crud->addField([
//            'name' => 'is_active',
//            'label' => 'Show Block on Homepage',
//            'type' => 'checkbox',
//            'tab' => 'General info',
//            'default' => 1
//        ]);
//
//    }

//    protected function setupUpdateOperation()
//    {
//        $this->setupCreateOperation();
//    }
}