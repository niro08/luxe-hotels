<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\CityRequest;

class CityCrudController extends CrudController {

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\City");
        $this->crud->setRoute(config('backpack.base.route_prefix') . "/city");
        $this->crud->setEntityNameStrings('city', 'cities');

    }

    protected function setupListOperation()
    {
        $this->crud->addColumn(['name' => 'title', 'type' => 'text', 'label' => 'Title']);
        $this->crud->addColumn(['name' => 'slug', 'type' => 'text', 'label' => 'Slug']);
        $this->crud->addColumn([
                'name' => 'province_id',
                'type' => 'select',
                'label' => 'Province',
                'entity' => 'province',
                'attribute' => 'title',
                'model' => 'App\Models\Province'
            ]);

        $this->crud->orderBy('title');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(CityRequest::class);

        $this->crud->addField([
            'name' => 'title',
            'label' => 'Title',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'province_id',
            'type' => 'select',
            'label' => 'Province',
            'entity' => 'province',
            'attribute' => 'title',
            'model' => 'App\Models\Province',
            'options'   => (function ($query) {
                return $query->orderBy('title', 'ASC')->get();
            }),
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'content',
            'label' => 'Content',
            'type' => 'wysiwyg',
            'tab' => 'General info'
        ]);


        foreach(['meta_title','meta_description','meta_keywords'] as $field){
            $label = ucfirst(str_replace('_',' ',$field));
            $this->crud->addField([
                'name' => $field,
                'label' => $label,
                'fake' => true,
                'type' => $field === 'meta_description' ? 'textarea' : 'text',
                'store_in' => 'meta',
                'tab' => 'Meta Info'
            ]);
        }


//        $this->crud->setFromDb();
//        $this->crud->removeField('slug');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}