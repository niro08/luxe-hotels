<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\BlockRequest;

class BlockCrudController extends CrudController {

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\Block");
        $this->crud->setRoute(config('backpack.base.route_prefix') . "/block");
        $this->crud->setEntityNameStrings('block', 'blocks');

    }

    protected function setupListOperation()
    {
        $this->crud->addColumn(['name' => 'title', 'type' => 'text', 'label' => 'Title']);

        $this->crud->addColumn(['name' => 'url', 'type' => 'text', 'label' => 'Url']);

        $this->crud->addColumn([
            // n-n relationship (with pivot table)
            'label' => "Hotels", // Table column heading
            'type' => "select_multiple",
            'name' => 'hotels', // the method that defines the relationship in your Model
            'entity' => 'hotels', // the method that defines the relationship in your Model
            'attribute' => "title", // foreign key attribute that is shown to user
            'model' => "App\Models\Hotel", // foreign key model
        ]);

        $this->crud->addColumn(['name' => 'sort_order', 'type' => 'number', 'label' => 'Sort Order']);

        $this->crud->orderBy('id');

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(BlockRequest::class);

        $this->crud->addField([
            'name' => 'title',
            'label' => 'Title',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'subtitle',
            'label' => 'Subtitle',
            'tab' => 'General info',
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Hotels",
            'type'      => 'select2_multiple',
            'name'      => 'hotels', // the method that defines the relationship in your Model
            'entity'    => 'hotels', // the method that defines the relationship in your Model
            'attribute' => 'title', // foreign key attribute that is shown to user
            'model'     => "App\Models\Hotel", // foreign key model
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'options'   => (function ($query) {
                return $query->orderBy('title', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'General info',
        ]);

        $this->crud->addField([   // URL
            'name' => 'url',
            'label' => 'View All Link',
            'type' => 'text',
            'hint' => 'e.g. "nederland/inspiratie/city-hotels"',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'sort_order',
            'type' => 'number',
            'label' => 'Sort Order',
            'tab' => 'General info'
        ]);

        $this->crud->addField([
            'name' => 'is_active',
            'label' => 'Show Block on Homepage',
            'type' => 'checkbox',
            'tab' => 'General info',
            'default' => 1
        ]);

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}