<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\HotelRequest;
use Gaspertrix\Backpack\DropzoneField\Traits\HandleAjaxMedia;


class HotelCrudController extends CrudController {

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use HandleAjaxMedia;

    public function setup()
    {
        $this->crud->setModel("App\Models\Hotel");
        $this->crud->setRoute(config('backpack.base.route_prefix') . "/hotel");
        $this->crud->setEntityNameStrings('hotel', 'hotels');

    }

    protected function setupListOperation()
    {
        $this->crud->addColumn(['name' => 'title', 'type' => 'text', 'label' => 'Title']);
        $this->crud->addColumn(['name' => 'title', 'type' => 'text', 'label' => 'Title']);
        $this->crud->addColumn(['name' => 'slug', 'type' => 'text', 'label' => 'Slug']);
        $this->crud->addColumn([
            'name' => 'city_id',
            'type' => 'select',
            'label' => 'City',
            'entity' => 'city',
            'attribute' => 'title',
            'model' => 'App\Models\City'
        ]);

        $this->crud->addColumn([
                // n-n relationship (with pivot table)
                'label' => "Categories", // Table column heading
                'type' => "select_multiple",
                'name' => 'categories', // the method that defines the relationship in your Model
                'entity' => 'categories', // the method that defines the relationship in your Model
                'attribute' => "title", // foreign key attribute that is shown to user
                'model' => "App\Models\Category", // foreign key model
        ]);

        $this->crud->addColumn([
            'name' => 'price',
            'type' => 'number',
            'decimals' => 2,
            'label' => 'Price',
            'suffix' => ' €'
        ]);

        $this->crud->orderBy('id');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(HotelRequest::class);

        $this->crud->addField([
            'name' => 'title',
            'label' => 'Title',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'short_description',
            'type' => 'textarea',
            'label' => 'Short Description',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'city_id',
            'type' => 'select',
            'label' => 'City',
            'entity' => 'city',
            'attribute' => 'title',
            'model' => 'App\Models\City',
            'options'   => (function ($query) {
                return $query->orderBy('title', 'ASC')->get();
            }),
            'tab' => 'General info',
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Categories",
            'type'      => 'select2_multiple',
            'name'      => 'categories', // the method that defines the relationship in your Model
            'entity'    => 'categories', // the method that defines the relationship in your Model
            'attribute' => 'title', // foreign key attribute that is shown to user
            'model'     => "App\Models\Category", // foreign key model
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'options'   => (function ($query) {
                return $query->orderBy('title', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'General info',
        ]);

        $this->crud->addField([    // Select2Multiple = n-n relationship (with pivot table)
            'label'     => "Blocks",
            'type'      => 'select2_multiple',
            'name'      => 'blocks', // the method that defines the relationship in your Model
            'entity'    => 'blocks', // the method that defines the relationship in your Model
            'attribute' => 'title', // foreign key attribute that is shown to user
            'model'     => "App\Models\Block", // foreign key model
            'pivot'     => true, // on create&update, do you need to add/delete pivot table entries?
            'options'   => (function ($query) {
                return $query->orderBy('title', 'ASC')->get();
            }), // force the related options to be a custom query, instead of all(); you can use this to filter the results show in the select
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'booking_link',
            'label' => 'Partner Link',
            'type' => 'url',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'price',
            'type' => 'number',
            'attributes' => ["step" => "any"],
            'label' => 'Price',
            'suffix' => ' €',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'label' => 'Link to video file on Youtube',
            'name' => "video",
            'type' => 'video',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'is_deal',
            'label' => 'Show Hotel in Deals section',
            'type' => 'checkbox',
            'tab' => 'General info',
        ]);


        foreach (['address','phone','rooms_number','floors_number','suites_number'] as $field){
            $label = ucfirst(str_replace('_',' ',$field));
            $this->crud->addField([
                'name' => $field,
                'label' => $label,
                'fake' => true,
                'store_in' => 'head_info',
                'tab' => 'Head Info'
            ]);
        }

        $this->crud->addField([
            'name' => 'content',
            'label' => 'Content',
            'type' => 'wysiwyg',
            'tab' => 'Content'
        ]);

        $this->crud->addField([
            'name' => 'services_info',
            'label' => 'All hotel services',
            'type' => 'wysiwyg',
            'tab' => 'Tabs'
        ]);

        $this->crud->addField([
            'name' => 'general_info',
            'label' => 'General info',
            'type' => 'wysiwyg',
            'tab' => 'Tabs'
        ]);

        foreach(['meta_title','meta_description','meta_keywords'] as $field){
            $label = ucfirst(str_replace('_',' ',$field));
            $this->crud->addField([
                'name' => $field,
                'label' => $label,
                'fake' => true,
                'type' => $field === 'meta_description' ? 'textarea' : 'text',
                'store_in' => 'meta',
                'tab' => 'Meta Info'
            ]);
        }

//        $this->crud->setFromDb();
//        $this->crud->removeField('slug');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

        $this->crud->addField([
            'name' => 'images',
            'label' => 'Gallery',
            'type' => 'dropzone_media',
            'collection' => 'hotels',
            'tab' => 'Content',
            'options' => [
                'thumbnailHeight' => 128,
                'thumbnailWidth' => 128,
                'maxFilesize' => 10,
                'addRemoveLinks' => true,
                'createImageThumbnails' => true,
                'acceptedFiles' => 'image/*'
            ]
        ]);

    }
}