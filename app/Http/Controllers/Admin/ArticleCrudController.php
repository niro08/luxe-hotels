<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\ArticleRequest;
use Gaspertrix\Backpack\DropzoneField\Traits\HandleAjaxMedia;

class ArticleCrudController extends CrudController
{

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;
    use HandleAjaxMedia;

    public function setup()
    {
        $this->crud->setModel('App\Models\Article');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/article');
        $this->crud->setEntityNameStrings('article', 'articles');

//        $this->crud->denyAccess('show');
//        $this->crud->setCreateView('blog::admin.create');
//        $this->crud->setEditView('blog::admin.edit');
    }

    protected function setupListOperation()
    {
        // ------ CRUD COLUMNS
        $this->crud->addColumn([
            'name' => 'title',
            'label' => 'Title',
        ]);

        $this->crud->addColumn([
            'name' => 'slug',
            'label' => 'Slug',
        ]);

        $this->crud->addColumn([
            'name' => 'author',
            'label' => 'Author',
        ]);
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ArticleRequest::class);
        // ------ PRIMARY FIELDS
        $this->crud->addField([
            'name' => 'title',
            'label' => 'Title',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'author',
            'label' => 'Author',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'slug',
            'label' => 'Slug',
            'hint' => 'Automatically created based on title if left empty.',
            'tab' => 'General info',
        ]);


        $this->crud->addField([
            'name' => 'content',
            'label' => 'Content',
            'type' => 'wysiwyg',
            'tab' => 'General info',
        ]);


        $this->crud->addField([
            'label' => 'Link to video file on Youtube',
            'name' => "featured_video",
            'type' => 'video',
            'tab' => 'General info',
        ]);

        foreach (['meta_title', 'meta_description', 'meta_keywords'] as $field) {
            $label = ucfirst(str_replace('_', ' ', $field));
            $this->crud->addField([
                'name' => $field,
                'label' => $label,
                'fake' => true,
                'type' => $field === 'meta_description' ? 'textarea' : 'text',
                'store_in' => 'meta',
                'tab' => 'Meta Info'
            ]);
        }
    }
    protected function setupUpdateOperation()
    {
        $this->crud->addField([
            'name' => 'images',
            'label' => 'Images',
            'type' => 'dropzone_media',
            'collection' => 'blogs',
            'hint' => 'First image will be display as thumbnail.',
            'tab' => 'General info',
            'options' => [
                'thumbnailHeight' => 128,
                'thumbnailWidth' => 128,
                'maxFilesize' => 10,
                'addRemoveLinks' => true,
                'createImageThumbnails' => true,
                'acceptedFiles' => 'image/*'
            ]
        ]);

        $this->setupCreateOperation();
    }
}