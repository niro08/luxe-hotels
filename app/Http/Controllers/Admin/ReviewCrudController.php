<?php namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use App\Http\Requests\ReviewRequest;

class ReviewCrudController extends CrudController {

    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\BulkDeleteOperation;

    public function setup()
    {
        $this->crud->setModel("App\Models\Review");
        $this->crud->setRoute(config('backpack.base.route_prefix') . "/review");
        $this->crud->setEntityNameStrings('review', 'reviews');

    }

    protected function setupListOperation()
    {
        $this->crud->addColumn(['name' => 'title', 'type' => 'text', 'label' => 'Title']);
        $this->crud->addColumn(['name' => 'name', 'type' => 'text', 'label' => 'Name']);
        $this->crud->addColumn(['name' => 'score', 'type' => 'text', 'label' => 'Score']);
        $this->crud->addColumn([
            'name' => 'hotel_id',
            'type' => 'select',
            'label' => 'Hotel',
            'entity' => 'hotel',
            'attribute' => 'title',
            'model' => 'App\Models\Hotel'
        ]);
        $this->crud->addColumn(['name' => 'created_at', 'type' => 'text', 'label' => 'Created At']);
        $this->crud->addColumn(['name' => 'approved', 'type' => 'boolean', 'label' => 'Approved']);

        $this->crud->orderBy('title');
    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(ReviewRequest::class);


        $this->crud->addField([
            'name' => 'hotel_id',
            'type' => 'select',
            'label' => 'Hotel',
            'entity' => 'hotel',
            'attribute' => 'title',
            'attributes' => [
                'readonly'    => 'readonly'
            ],
            'model' => 'App\Models\Hotel',
            'options'   => (function ($query) {
                return $query->orderBy('title', 'ASC')->get();
            }),
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'score',
            'label' => 'Score',
            'attributes' => [
                'readonly' => 'readonly'
            ],
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'title',
            'label' => 'Title',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'body',
            'label' => 'Body',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'name',
            'label' => 'Name',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'email',
            'label' => 'Email',
            'tab' => 'General info',
        ]);

        $this->crud->addField([
            'name' => 'approved',
            'label' => 'Approved',
            'type' => 'checkbox',
            'tab' => 'General info',
        ]);



//        $this->crud->setFromDb();
//        $this->crud->removeField('slug');
    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}