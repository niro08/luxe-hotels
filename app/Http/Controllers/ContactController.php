<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\Setting;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function getContact() {

        $text = Setting::get('contact_us_text') ?? "<strong>Heb je een vraag?</strong>
                <p>Stel je vraag gemakkelijk via het contactformulier en wij proberen je vraag binnen 24 uur
                te beantwoorden.</p>";
        return view('contact_us',compact('text'));
    }

    public function saveContact() {

        $this->validateContact();

        $contact = new Contact(request(['name','email','phone_number','message']));

        $contact->save();

        return back()->with('success', 'Thank you for contact us!');

    }

    protected function validateContact(){
        return request()->validate([
            'name' => 'required',
            'email' => 'required|email',
            'message' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);
    }
}
