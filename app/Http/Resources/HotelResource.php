<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HotelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'short_description' => $this->short_description,
            'thumb' => $this->getThumb(),
            'price' => $this->price,
            'rating' => $this->getRating(),
            'reviewsCount' => $this->getReviewsCount(),
            'address' => $this->head_info['address'],
        ];
    }
}
