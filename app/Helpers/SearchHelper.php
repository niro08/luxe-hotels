<?php

namespace App\Helpers;

use App\Models\Category;
use App\Models\Hotel;
use App\Models\City;
use App\Models\Province;

class SearchHelper
{
    static function getResult(){
        $result = [];
        if($query = request('query')){
            $hotels = Hotel::where('title','LIKE','%' . request('query') . '%')->get()->pluck('title','slug');
            $cities = City::has('hotels')->where('title','LIKE','%' . request('query') . '%')->get()->pluck('title','slug');
            $provinces = Province::has('hotels')->where('title','LIKE','%' . request('query') . '%')->get()->pluck('title','slug');
            $categories = Category::has('hotels')->where('title','LIKE','%' . request('query') . '%')->get()->pluck('title','slug');
            foreach(['Hotels' => $hotels, 'Cities' => $cities, 'Provinces' => $provinces,'Categories' => $categories] as $key => $item){
               if(count($item)){
                   $result[$key] = $item;
               }
            }
            if(empty($result)){
                $result['error'] = 'geen resultaten, probeer opnieuw';
            }
        }
        return $result;
    }
}