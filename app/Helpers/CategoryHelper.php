<?php

namespace App\Helpers;

use App\Models\Setting;

/**
 * Class CategoryHelper
 * @package App\Helpers
 */
class CategoryHelper
{
    /**
     * @var array
     */
    private $props = [];

    private const SHOWALL_TITLE = 'Alle Hotels in Nederland';
    private const CURRENT_FILTERS_PROP = 'currentFilters';
    private const MODEL_PROP = 'model';
    private const MODELS_NAMESPACE = "App\\Models\\";


    /**
     * @var
     */
    private static $instance;

    /**
     * CategoryHelper constructor.
     */
    private function __construct()
    {

    }

    /**
     * @return CategoryHelper
     */
    public static function getlnstance()
    {
        if (empty(self::$instance)) {
            self::$instance = new CategoryHelper();
        }
        return self::$instance;
    }

    /**
     * @param string $key
     * @return mixed|null
     */
    public function getProperty(string $key)
    {
        return isset($this->props[$key]) ? $this->props[$key] : null ;
    }

    /**
     * @param string $key
     * @param $val
     */
    public function setProperty(string $key, $val)
    {
        $this->props[$key] = $val;
    }

    /**
     * @param array $properties
     */
    public function setProperties(array $properties)
    {
        foreach ($properties as $key => $val)
            $this->props[$key] = $val;
    }

    /**
     * @return string
     */
    public function getTitle()
    {

        $result = self::SHOWALL_TITLE;
        if ($currentFilters = $this->getProperty(self::CURRENT_FILTERS_PROP)) {
            if ($this->_isAvailable('city')) {
                $cityModel = $this->getModel('city','id',$currentFilters['city']);
                $result = "Hotels in {$cityModel->title}, {$cityModel->province->title}";
                if ($this->_isAvailable('category')) {
                    $categoryModel = $this->getModel('category','id', $currentFilters['category']);
                    $result = $categoryModel->title . str_replace("Hotels", "", $result);
                }
            } elseif ($this->_isAvailable('province')) {
                $provinceModel = $this->getModel('province','id',$currentFilters['province']);
                $result = "Hotels in {$provinceModel->title}";
                if ($this->_isAvailable('category')) {
                    $categoryModel = $this->getModel('category','id', $currentFilters['category']);
                    $result = $categoryModel->title . str_replace("Hotels", "", $result);
                }
            } elseif ($this->_isAvailable('category')) {
                $categoryModel = $this->getModel('category','id', $currentFilters['category']);
                $result = $categoryModel->title;
                if ($this->_isAvailable('city')) {
                    $cityModel = $this->getModel('city','id',$currentFilters['city']);
                    $result .= " in {$cityModel->title}, {$cityModel->province->title}";
                } elseif ($this->_isAvailable('province')) {
                    $provinceModel = $this->getModel('province','id',$currentFilters['province']);
                    $result .= " in {$provinceModel->title}";
                }
            }
        }
        return $result;
    }

    /**
     * @return array|null
     */
    public function getCurrentFiltersIds()
    {
        $currentFilters = $this->getProperty(self::CURRENT_FILTERS_PROP);

        return $currentFilters ?
            array_map(function ($e) {
                return (string)$e ?? '';
            }, $currentFilters) : null;
    }

    /**
     * @return array
     */
    public function getMeta()
    {
        $meta = [
            'meta_title' => Setting::get('allhotels_meta_title'),
            'meta_description' => Setting::get('allhotels_meta_description'),
            'meta_keywords' => Setting::get('allhotels_meta_keywords')
        ];
        if ($model = $this->getProperty(self::MODEL_PROP)) {
            $meta = $model->meta;
        }
        return $meta;
    }

    /**
     * @param $key
     * @return bool
     */
    private function _isAvailable($key)
    {
        return !!$this->getProperty(self::CURRENT_FILTERS_PROP)[$key];
    }

    /**
     * @param $modelKey
     * @param $attr
     * @param $value
     * @return mixed
     */
    public function getModel($modelKey, $attr, $value)
    {
        $className = self::MODELS_NAMESPACE . ucfirst($modelKey);
        $model = $className::where($attr, $value)->with('hotels')->firstOrFail();
        return $model;
    }

}