<?php

namespace App\Providers;

use App\Models\Category;
use App\Models\City;
use App\Models\Province;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class AppServiceProvider extends ServiceProvider
{

    protected $navModels = ['categories' => Category::class,'provinces' => Province::class,'cities' => City::class];
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Collection::macro('filterHotelsCollection', function ($filterName,$value,$isAjax = false) {
            return $this->filter(function ($hotel) use ($filterName,$value,$isAjax) {
                $key = $isAjax ? 'id' : 'slug';
                return $filterName === 'category' ?
                    in_array($value, $hotel->$filterName()->pluck($key)->toArray()) :
                    $hotel->$filterName()->$key === $value;
            });
        });

        Collection::macro('getFilterOptions', function ($filterName) {
            $result = null;
            switch ($filterName){
                case 'province':
                    $result = $this->pluck("city.$filterName.title","city.$filterName.id");
                    break;
                case 'category':
                    $result = $this->pluck('categories')->collapse()->pluck('title','id');
                    break;
                default:
                    $result = $this->pluck("$filterName.title","$filterName.id");
                    break;
            }
            return $result->toArray();
        });

        $navData = [];

        //Items for Navigation Menu
        foreach ($this->navModels as $key => $model){
                $navData[$key] = $model::has('hotels')->get()->sortBy('title')->pluck('title','slug');
        }
        view()->composer(
          ['layouts.app'],
            function ($view) use ($navData){
              $view->with('navData',$navData);
            }
        );
    }
}
