<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use \Carbon\Carbon;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Article extends Model implements HasMedia
{
    use CrudTrait, Sluggable, SluggableScopeHelpers, GetContentAttributeTrait, HasMediaTrait;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'articles';
    protected $primaryKey = 'id';
    protected $fillable = ['title', 'slug', 'content', 'author', 'featured_video','meta'];

    protected $casts = [
        'meta' => 'array',
        'images' => 'array',
        'featured_video' => 'array',
    ];
    protected $fakeColumns = ['meta'];


    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'slug_or_title',
            ],
        ];
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
    */

    // The slug is created automatically from the "name" field if no slug exists.
    public function getSlugOrTitleAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }

        return $this->title;
    }

//    public function getSlugAttribute($value)
//    {
//        if(strpos($value,'/blog/') === false) $value = "/blog/$value";
//
//        return $value;
//    }

    // cut the content down to a brief summary.
    public function getSummaryAttribute()
    {
        return str_limit(strip_tags($this->content), 150);
    }

    // Allow the option to have a cleaner text display the published date rather then just a date.
    public function getPublishedAttribute()
    {

        // less then 10 hours
        if(Carbon::parse($this->created_at) > Carbon::now()->subHours(10)) {
            return Carbon::parse($this->created_at)->diffForHumans();
        }

        // posted today
        if(Carbon::parse($this->created_at)->day == Carbon::now()->day) {
            return 'today at ' . Carbon::parse($this->created_at)->format('h:i');
        }

        return 'on ' . Carbon::parse($this->created_at)->format('d M Y');
    }

    /*
    |
    | FUNCTIONS
    |
    */
    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(420)
            ->height(280);
    }

    public function getImagePathArray(){
        $result = [];
        if($mediaCollection = $this->getMedia('blogs')){
            foreach ($mediaCollection as $item){
                $result[] = $item->getUrl();
            }
        }
        return $result;
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */

}