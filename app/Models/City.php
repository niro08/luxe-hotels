<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class City extends Model implements CategoryViewInterface
{
    use Sluggable,CrudTrait,GetContentAttributeTrait;

    protected $casts = [
        'meta' => 'array',
    ];

    protected $fakeColumns = ['meta'];
    protected $hidden = ['slug'];
    protected $guarded = ['id'];

    public function hotels()
    {
        return $this->hasMany(Hotel::class);
    }

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
            ]
        ];
    }

    public function getSlugAttribute($value)
    {
        if($this->province){
            $value = $this->province->slug . "/" . $value;
        }
        return $value;
    }
}
