<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Page extends Model
{
    use CrudTrait,GetContentAttributeTrait;

    protected $guarded = ['id'];

    protected $casts = [
        'meta' => 'array',
    ];

    protected $fakeColumns = ['meta'];

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
