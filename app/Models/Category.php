<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Category extends Model implements CategoryViewInterface,HasMedia
{
    use Sluggable,CrudTrait,GetContentAttributeTrait,HasMediaTrait;

    protected $casts = [
        'meta' => 'array',
    ];

    protected $fakeColumns = ['meta'];
    protected $hidden = ['slug'];
    protected $guarded = ['id'];

    public function hotels()
    {
        return $this->belongsToMany(
            Hotel::class,
            'hotels_categories',
            'category_id',
            'hotel_id'
        );
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getSlugAttribute($value)
    {
        $value = "/nederland/inspiratie/$value";
        return $value;
    }
}
