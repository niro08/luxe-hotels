<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Review extends Model
{
    use CrudTrait;

    protected $fillable = ['name', 'email', 'score', 'title', 'body','hotel_id','approved'];
    //
    public function hotel(){
        return $this->belongsTo(Hotel::class);
    }
}
