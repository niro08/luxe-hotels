<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;

class Hotel extends Model implements HasMedia
{
    use Sluggable,CrudTrait,HasMediaTrait,GetContentAttributeTrait;

    protected $casts = [
        'meta' => 'array',
        'head_info' => 'array',
        'images' => 'array',
        'video' => 'array'
    ];

    protected $fakeColumns = ['meta','head_info'];
    protected $hidden = ['slug'];
    protected $guarded = ['id'];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function province()
    {
        return $this->city->province;
    }

    public function reviews(){
        return $this->hasMany(Review::class);
    }

    public function approvedReviews(){
        return $this->reviews()->where('approved','=',1);
    }

    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'hotels_categories',
            'hotel_id',
            'category_id'
        );
    }

    public function blocks()
    {
        return $this->belongsToMany(
            Block::class,
            'hotels_blocks',
            'hotel_id',
            'block_id'
        );
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(350)
            ->height(230);
    }

    public function getImagePathArray(){
        $result = [];
        if($mediaCollection = $this->getMedia('hotels')){
            foreach ($mediaCollection as $item){
                $result[] = $item->getUrl();
            }
        }
        return $result;
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getSlugAttribute($value)
    {
        $value = "/hotel/$value";
        return $value;
    }

    public function getThumb(){
        return $this->getFirstMedia('hotels') ?
            $this->getFirstMedia('hotels')->getUrl('thumb') :
            "/images/no_image.svg";
    }

    public function getRating(){
        $reviews = $this->approvedReviews()->get();
        return count($reviews) ? $reviews->sum('score')/count($reviews) : 0;
    }

    public function getReviewsCount(){
        $reviews = $this->approvedReviews()->get();
        return count($reviews);
    }
}
