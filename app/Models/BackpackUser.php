<?php

namespace App\Models;

use App\User;
use Backpack\CRUD\app\Models\Traits\InheritsRelationsFromParentModel;

class BackpackUser extends User
{
    use InheritsRelationsFromParentModel;

    protected $table = 'users';

}
