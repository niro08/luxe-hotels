<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Block extends Model
{
    use CrudTrait;

    protected $guarded = ['id'];

    public function hotels()
    {
        return $this->belongsToMany(
            Hotel::class,
            'hotels_blocks',
            'block_id',
            'hotel_id'
        );
    }
}
