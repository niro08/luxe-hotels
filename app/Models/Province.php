<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait;


class Province extends Model implements CategoryViewInterface
{
    protected $casts = ['meta' => 'array'];
    protected $fakeColumns = ['meta'];
    protected $table = 'provinces';
    protected $hidden = ['slug'];
    protected $guarded = ['id'];

    use Sluggable,CrudTrait,GetContentAttributeTrait;

    public function cities(){
        return $this->hasMany(City::class);
    }

    public function hotels()
    {
        return $this->hasManyThrough(Hotel::class, City::class);
    }

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    public function getSlugAttribute($value)
    {
        $value = "/nederland/$value";
        return $value;
    }
}
