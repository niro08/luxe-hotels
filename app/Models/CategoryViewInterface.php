<?php
/**
 * Created by Egor Becker.
 * Date: 2020-02-28
 * Time: 11:15
 */
namespace App\Models;

interface CategoryViewInterface{

    public function hotels();

}