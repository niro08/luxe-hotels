<?php

namespace App\Models;

trait GetContentAttributeTrait
{
    public function getContentAttribute($value)
    {
        $value = str_replace("<h1", "<h1 class='is-1 title has-text-weight-normal'",$value);
        $value = str_replace("<h2", "<h2 class='is-2 title has-text-weight-normal'",$value);
        $value = str_replace("<h3", "<h3 class='is-3 title has-text-weight-normal'",$value);
        return $value;
    }
}