<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index');
Route::get('nederland','CategoryController@show');
Route::get('nederland/inspiratie','CategoryController@inspiration');
Route::get('nederland/inspiratie/{category}/{province?}/{city?}','CategoryController@show');
Route::get('nederland/deals','CategoryController@show');
Route::get('nederland/{province?}/{city?}/{category?}','CategoryController@show');
Route::get('hotel/{slug}','HotelController@show');
Route::get('hotel/{slug}/book','HotelController@book');
Route::get('contact-us', 'ContactController@getContact');
Route::post('contact-us', 'ContactController@saveContact');
Route::get('/blog','ArticleController@index');
Route::get('/blog/{article}','ArticleController@show');
Route::get('/{page}','PageController@index');
