<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['web', config('backpack.base.middleware_key', 'admin')],
    'namespace' => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('province', 'ProvinceCrudController');
    Route::crud('city', 'CityCrudController');
    Route::crud('category', 'CategoryCrudController');
    Route::crud('hotel', 'HotelCrudController');
    Route::crud('block', 'BlockCrudController');
    Route::crud('contact', 'ContactCrudController');
    Route::crud('page', 'PageCrudController');
    Route::crud('setting', 'SettingCrudController');
    Route::crud('article', 'ArticleCrudController');
    Route::crud('review', 'ReviewCrudController');
    Route::post('hotel/{id}/media', 'HotelCrudController@uploadMedia');
    Route::delete('hotel/{id}/media/{mediaId}', 'HotelCrudController@deleteMedia');
    Route::post('hotel/{id}/media/reorder', 'HotelCrudController@reorderMedia');
    Route::post('category/{id}/media', 'CategoryCrudController@uploadMedia');
    Route::delete('category/{id}/media/{mediaId}', 'CategoryCrudController@deleteMedia');
    Route::post('category/{id}/media/reorder', 'CategoryCrudController@reorderMedia');
    Route::post('article/{id}/media', 'ArticleCrudController@uploadMedia');
    Route::delete('article/{id}/media/{mediaId}', 'ArticleCrudController@deleteMedia');
    Route::post('article/{id}/media/reorder', 'ArticleCrudController@reorderMedia');
}); // this should be the absolute last line of this file