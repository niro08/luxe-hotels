require('./bootstrap');
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueRouter from 'vue-router'
import VueYouTubeEmbed from 'vue-youtube-embed'

Vue.use(VueAwesomeSwiper);
Vue.use(VueRouter);
Vue.use(VueYouTubeEmbed);

Vue.component('navigation-component', require('./components/NavigationComponent.vue').default);
Vue.component('gallery-component', require('./components/GalleryComponent.vue').default);
Vue.component('search-component', require('./components/SearchComponent.vue').default);
Vue.component('notification-component', require('./components/NotificationComponent.vue').default);
Vue.component('map-component', require('./components/MapComponent.vue').default);
Vue.component('review-component', require('./components/ReviewComponent.vue').default);
Vue.component('category-component', require('./components/CategoryComponent.vue').default);
Vue.component('popup-video-component', require('./components/PopupVideoComponent.vue').default);

const app = new Vue({
    el: '#app',
});