@extends('layouts.app')
@section('body-class','hotel-page')
@section('meta_title',$hotel->meta['meta_title'] ?? $hotel->title)
@section('meta_keywords',$hotel->meta['meta_keywords'] ?? '')
@section('meta_description',$hotel->meta['meta_description'] ?? $hotel->short_description)
@section('rich_snippets')
    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "WebPage",
      "site_name": "Luxe-Hotels.nu",
      "title": "{{ $hotel->meta['meta_title'] ?? $hotel->title }}",
      "description": "{{ $hotel->meta['meta_description'] ?? $hotel->short_description }}",
      "image": "{{ count($gallery) ? $gallery[0] : '' }}",
      "url": "{{ url()->current() }}"
    }
    </script>
@endsection
@section('content')
    <section class="section">
        <h2 class="title is-2 is-marginless has-text-centered has-text-weight-normal">
            {{ $hotel->title }}
        </h2>
        @if(count($gallery))
        <gallery-component :gallery="{{ json_encode($gallery) }}"></gallery-component>
        @endif
        <div class="head-info has-text-centered">
            <p>Aantal kamers: {{ $hotel->head_info['rooms_number'] }}, Aantal suites: {{ $hotel->head_info['suites_number'] }}, Aantal verdiepingen: {{ $hotel->head_info['floors_number']  }}</p>
            <p>Address: {{ $hotel->head_info['address'] }}</p>
        </div>
    </section>
    <section class="section" id="content-section">
        <div class="content-wrapper has-text-centered">

            <h2 class='is-2 title has-text-weight-normal'>
            {{ $hotel->title}} {{!strpos($hotel->title,$hotel->city->title) ? $hotel->city->title : "" }}
            </h2>

            <div class="has-text-right is-margin-bottom">
                @if($hotel->video)
                <popup-video-component :id="'{{ $hotel->video['id'] }}'"></popup-video-component>
                @endif
                <a class="button has-background-link is-size-5 is-size-6-mobile has-text-white align-content-end"
                   href="{{ $hotel->slug . '/book' }}">Bekijk prijzen</a>
            </div>

            {!! $hotel->content !!}

            <a class="button has-background-link is-custom-link is-large has-text-center has-text-white align-content-end"
               href="{{ $hotel->slug . '/book' }}">Bekijk prijzen</a>

            @if($hotel->head_info['address'])
                <section class="section" id="map-section">
                <h3 class="title is-3 is-marginless has-text-centered has-text-weight-normal">
                Locatie & route
                </h3>
                <map-component :address="'{{ $hotel->head_info['address'] }}'"></map-component>
                </section>
            @endif
        </div>
    </section>

    <section class="section" id="review-section">
        <div class="content-wrapper has-text-centered">
            <?php
            $captcha = [
                'id' => 'buzzNoCaptchaId_' . md5(uniqid(rand(), true)),
                'sitekey' => config('captcha.sitekey','6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI')
            ];
            ?>
            <review-component :reviews="{{ json_encode($reviews) }}" :captcha="{{ json_encode($captcha) }}" :hotel="{{ json_encode(['id' => $hotel->id, 'title' => $hotel->title]) }}"></review-component>
        </div>
    </section>
@endsection