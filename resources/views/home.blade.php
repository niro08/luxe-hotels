@extends('layouts.app')
@section('body-class','home-page')
@section('meta_title',$meta['title'])
@section('meta_keywords',$meta['keywords'])
@section('meta_description',$meta['description'])
@section('rich_snippets')
    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "WebPage",
      "site_name": "Luxe-Hotels.nu",
      "title": "{{ $meta['title'] }}",
      "description": "{{ $meta['description'] }}",
      "image": "{{ env('APP_URL') . "/images/logo.png" }}",
      "url": "{{ url()->current() }}"
    }
    </script>
@endsection
@section('content')
    @if(count($blocks))
    <section class="section" id="blocks-section">
        @foreach($blocks as $block)
            <div class="block-wrapper">
            <div class="title-wrapper has-text-centered">
                    <h3 class="title is-3 has-text-weight-normal is-uppercase">{{ $block->title }}</h3>
                    <h6 class="subtitle is-6 is-uppercase is-muted">{{ $block->subtitle }}</h6>
                </div>
                    <div class="columns is-8 is-variable is-multiline">
                        @foreach($block->hotels as $hotel)
                            <div class="column is-12-mobile is-6-tablet is-4-desktop">
                                <div class="card is-shadowless">
                                    <div class="card-image">
                                        <figure class="image">
                                            <a href="{{ $hotel->slug }}">
                                                <img src="{{
                                        $hotel->hasMedia('hotels') ? $hotel->getFirstMedia('hotels')->getUrl('thumb') : ''
                                        }}" alt="{{ $hotel->title }}">
                                                <span class="is-overlay block-title">{{ $hotel->title }}</span>
                                            </a>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="has-text-right has-text-black">
                            <a href="{{ $block->url }}">Bekijk alle {{ $block->title }}</a>
                    </div>
                </div>
        @endforeach
    </section>
    @endif
    @if(isset($content))
        <section class="section" id="content-section">
            {!! $content ?? '' !!}
        </section>
    @endif
@endsection
