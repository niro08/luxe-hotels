@extends('layouts.app')
@section('body-class','contact-us')
@section('meta_title','Mail Ons')
@section('content')
<section class="section">
    @if(session()->has('success'))
        <notification-component message="{{ session('success') }}"></notification-component>
    @endif
    <div class="columns">
        <div class="column is-6">
            <h2 class="title is-2 is-family-primary has-text-weight-normal">
                Mail ons
            </h2>
            <form method="post" action="contact-us">
                @csrf()
                <div class="field">
                    <label for="name" class="label">Naam <span class="has-text-danger">*</span></label>
                    <div class="control has-icons-left">
                        <input class="input @error('name') is-danger @enderror" name="name" type="text">
                        <span class="icon is-small is-left">
                            <i class="fas fa-user"></i>
                        </span>
                    </div>
                    @error('name')
                    <p class="help is-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>
                <div class="field">
                    <label for="email" class="label">E-mailadres <span class="has-text-danger">*</span></label>
                    <div class="control has-icons-left">
                        <input class="input @error('email') is-danger @enderror" name="email" type="text">
                        <span class="icon is-small is-left">
                            <i class="fas fa-envelope"></i>
                        </span>
                    </div>
                    @error('email')
                    <p class="help is-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>
                <div class="field">
                    <label for="phone_number" class="label">Telefoonnummer</label>
                    <div class="control has-icons-left">
                        <input class="input" name="phone_number" type="text">
                        <span class="icon is-small is-left">
                            <i class="fas fa-phone"></i>
                        </span>
                    </div>
                    @error('phone_number')
                    <p class="help is-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>
                <div class="field">
                    <label for="message" class="label">Stel hier je vraag <span class="has-text-danger">*</span></label>
                    <div class="control">
                        <textarea class="textarea @error('message') is-danger @enderror" name="message" type="text"></textarea>
                    </div>
                    @error('message')
                    <p class="help is-danger" role="alert">{{ $message }}</p>
                    @enderror
                </div>
                <div class="field">
                    <div class="control">
                        {!! app('captcha')->display(['addJs' => false]) !!}
                    </div>
                    @error('g-recaptcha-response')
                    <p class="help is-danger" role="alert">Please check "I'm not a robot" checkbox</p>
                    @enderror
                </div>
                <div class="field">
                    <div class="control is-pulled-right">
                        <button class="button is-link">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="column is-6">
            <h2 class="title is-2 is-family-primary has-text-weight-normal">
                Contact
            </h2>
            <div class="contact-info">
                {!! $text !!}
            </div>
        </div>
    </div>
</section>
@endsection