@extends('layouts.app')
@section('body-class','blog-list-page')
@section('meta_title',$meta['title'] ?? 'Luxe-Hotels Blog')
@section('meta_keywords',$meta['keywords'])
@section('meta_description',$meta['description'])
@section('content')
    <section class="section">
        <div class="container">
            <div class="title-wrapper has-text-centered">
                <h2 class="title is-2 is-spaced">
                    Luxe-Hotels Blog
                </h2>
            </div>
            <div class="content-wrapper has-text-centered is-unselectable">
                @if(!count($articles))
                    <h3 class="title is-3 is-marginless has-text-centered has-text-weight-normal">
                    Sorry, blog artikelen niet gevonden...</h3>
                @else
                    <div class="columns is-variable is-multiline">
                        @foreach($articles as $article)
                            <div class="column is-12-mobile is-inline-flex-mobile is-4-tablet is-4-desktop">
                                <div class="card is-shadowless">
                                    <div class="card-image">
                                        <figure class="image">
                                            <a href="/blog/{{ $article->slug }}">
                                                <img src="{{ url(
                                        $article->hasMedia('blogs') ? $article->getFirstMedia('blogs')->getUrl('thumb') :
                                        '/images/no_image.svg'
                                        ) }}"
                                                     alt="{{ $article->title }}">
                                            </a>
                                        </figure>
                                        <div class="blog-title-wrapper">
                                        <a href="/blog/{{ $article->slug }}">
                                        <span class="has-text-weight-bold has-text-black blog-title is-size-5">{{ $article->title }}</span>
                                        </a>
                                        </div>
                                        <div class="blog-info-wrapper is-pulled-left">
                                            <span class="has-text-primary is-uppercase is-size-7">{{ $article->author ?? "Luxe-Hotels" }}</span>
                                            <span class="has-text-grey is-size-6">{{ $article->published ?? '' }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endif

            </div>
        </div>
    </section>
@endsection