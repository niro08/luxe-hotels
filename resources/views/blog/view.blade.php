@extends('layouts.app')
@section('body-class','blog-view-page')
@section('meta_title',$article->meta['meta_title'] ?? 'Luxe-Hotels Inspiratie')
@section('meta_keywords',$article->meta['meta_keywords'] ?? '')
@section('meta_description',$article->meta['meta_description'] ?? '')
@section('rich_snippets')
    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "BlogPosting",
      "mainEntityOfPage": {
        "@type": "WebPage",
        "@id": "{{ url()->current() }}"
      },
      "headline": "{{ $article->title }}",
      "image": [@php
        $string = '';
            foreach($gallery as $image){
            $string .= '"' . url('/') . $image . '",';
            }
        $string = trim($string,',');
        echo $string;
        @endphp],
      "datePublished": "{{ $article->created_at }}",
      "dateModified": "{{ $article->updated_at }}",
      "author": {
        "@type": "Person",
        "name": "{{ $article->author }}"
      },
       "publisher": {
        "@type": "Organization",
        "name": "Luxe-Hotels.nu",
        "logo": {
          "@type": "ImageObject",
          "url": "{{ url('/') . '/images/logo.png' }}"
        }
      }
    }
    </script>
@endsection
@section('content')
    @php
        $content = $article->content;
        $video = $article->featured_video ?? false;
        if($video && $video['id'] && strpos($content,'<p>!featured_video!</p>') !== false){
        $src = $video['provider'] === 'youtube' ?
        "https://www.youtube.com/embed/{$video['id']}" : "https://player.vimeo.com/video/{$video['id']}";
        $embed = '<div class="video-container"><iframe
        width="714"
        height="420"
        src="'.$src.'"
        frameborder="0"
        allow="accelerometer;
        autoplay;
        encrypted-media;
        gyroscope;
        picture-in-picture"
        allowfullscreen
        ></iframe></div>';
            $content = str_replace('<p>!featured_video!</p>',$embed,$content);

        }
    @endphp
    <section class="section">
        <h1 class="title is-1 has-text-centered has-text-weight-normal">
            {{ $article->title }}
        </h1>
        @if(count($gallery))
            <gallery-component :gallery="{{ json_encode($gallery) }}"></gallery-component>
        @endif
        @if($article->author)<span class="has-margin-right"><span class="has-text-grey">Author: </span>{{ $article->author }}</span>@endif
        <span><span class="has-text-grey">Published</span> {{ $article->published }}</span>
    </section>
    <section class="section" id="content-section">
        <div class="content-wrapper">
            {!! $content !!}
        </div>
    </section>
@endsection