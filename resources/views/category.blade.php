@extends('layouts.app')
@section('body-class','category-page')
@section('meta_title',$meta['meta_title'] ?? $title ?? '')
@section('meta_description',$meta['meta_description'] ?? $content ?? $title ?? '')
@section('meta_keywords',$meta['meta_keywords'] ?? '')
@section('rich_snippets')
    <script type="application/ld+json">
    {
      "@context": "https://schema.org/",
      "@type": "WebPage",
      "site_name": "Luxe-Hotels.nu",
      "title": "{{ $meta['meta_title'] ?? '' }}",
      "url": "{{ url()->current() }}"
    }
    </script>
@endsection
@section('content')
        <category-component :category_data="{{ json_encode(['filters' => $filters,'defaultFilters' => $defaultFilters, 'hotels' => $hotels, 'title' => $title, 'content' => $content ?? '']) }}"></category-component>
@endsection
