@extends('layouts.app')
@section('body-class','inspiration-page')
@section('meta_title',$meta['title'] ?? 'Luxe-Hotels Inspiratie')
@section('meta_keywords',$meta['keywords'])
@section('meta_description',$meta['description'])
@section('content')
    <section class="section">
        <div class="container">
            <div class="title-wrapper has-text-centered">
                <h2 class="title is-2 is-spaced">Inspiratie</h2>
            </div>
            <div class="columns is-variable is-multiline">
                    @foreach($categories as $category)
                        <div class="column is-12-mobile is-inline-flex-mobile is-4-tablet is-4-desktop">
                            <div class="card is-shadowless">
                                <div class="card-image">
                                    <figure class="image">
                                        <a href="{{ $category->slug }}">
                                            <img src="{{ url(
                                        $category->hasMedia('categories') ? $category->getFirstMedia('categories')->getUrl() :
                                        '/images/no_image.svg'
                                        ) }}"
                                        alt="{{ $category->title }}">
                                            <span class="is-overlay block-title">{{ $category->title }}</span>
                                        </a>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
        </div>
    </section>
@endsection