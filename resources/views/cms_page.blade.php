@extends('layouts.app')
@section('body-class',"$page->slug-page")
@section('meta_title',$page->meta['meta_title'] ?? $page->title)
@section('meta_keywords',$page->meta['meta_keywords'])
@section('meta_description',$page->meta['meta_description'])
@section('content')
    <section class="section">
        <h2 class="title has-text-centered is-2 is-family-primary has-text-weight-normal">
            {{ $page->title }}
        </h2>
        {!! $page->content !!}
    </section>
@endsection