<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('province') }}"><i class="fa fa-landmark nav-icon"></i> Province</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('city') }}"><i class="fa fa-landmark nav-icon"></i> City</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('category') }}"><i class="fa fa-landmark nav-icon"></i> Category</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('hotel') }}"><i class="fa fa-landmark nav-icon"></i> Hotel</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('block') }}"><i class="fa fa-landmark nav-icon"></i> Homepage Blocks</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('contact') }}"><i class="fa fa-landmark nav-icon"></i> Contact Us Requests</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('page') }}"><i class="fa fa-landmark nav-icon"></i>Custom Pages</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('article') }}"><i class="fa fa-landmark nav-icon"></i>Blog Pages</a></li>
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('review') }}"><i class="fa fa-landmark nav-icon"></i>Reviews</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('setting') }}'><i class='nav-icon fa fa-cog'></i> <span>Global Settings</span></a></li>
{{--<li class=nav-item><a class=nav-link href="{{ backpack_url('elfinder') }}"><i class="nav-icon fa fa-files-o"></i> <span>{{ trans('backpack::crud.file_manager') }}</span></a></li>--}}