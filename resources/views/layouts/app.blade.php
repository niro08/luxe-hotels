<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="c31e10499757136" content="51b248bbef0fd18fef34f87300b3b51c"/>
    @if (!app()->environment('production'))
        <meta name="robots" content="noindex,nofollow">
    @else
        <meta name="robots" content="index,follow">
    @endif
    <title>@yield('meta_title','Luxe Hotels')</title>
    <meta name="title" content="@yield('meta_title','Luxe Hotels')">
    <meta name="keywords" content="@yield('meta_keywords','')">
    <meta name="description" content="@yield('meta_description','')">

    @yield('rich_snippets','')
<!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://www.google.com/recaptcha/api.js?hl=nl" async defer></script>
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    @if (app()->environment('production'))
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-162286600-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];

            function gtag() {
                dataLayer.push(arguments);
            }

            gtag('js', new Date());
            gtag('config', 'UA-162286600-1');
        </script>
    @endif
</head>
<body class="page-layout @yield('body-class')">
<div id="app" class="container is-widescreen">

    <navigation-component :navigation="{{ json_encode($navData) }}"></navigation-component>

    <search-component></search-component>

    <main class="main" id="content">
        <div class="wrapper">
            @yield('content')
        </div>
    </main>
</div>
@php
    $footerLinks = [
    'Category' => [
    ['link' => '/nederland/inspiratie/romantische-hotels','title' => 'Romantic'],
    ['link' => '/nederland/inspiratie/city-hotels','title'  => 'Citytrip'],
    ['link' => '/nederland/inspiratie/business-hotels','title'  => 'Business'],
    ['link' => '/nederland/deals','title'  => 'Deals']
    ],
    'Zoeken' => [
    //['link' => '#','title' => 'Op provincie'],
    //['link' => '#','title' => 'Op stad'],
    ['link' => '/nederland/inspiratie/top-10-luxe-hotels','title' => 'Top 10 Luxe hotels'],
    ],
    'Luxe-Hotels' => [
    ['link' => '/over-ons','title' => 'Over ons'],
    ['link' => '/contact-us','title' => 'Contact'],
    ]
    ];
@endphp
<footer class="footer is-uppercase">
    <div class="container">
        <div class="columns is-multiline">
            <div class="column is-3 is-hidden-mobile"></div>
            @foreach($footerLinks as $title => $columns)
                <div class="column is-2 is-4-mobile {{ $title === 'Category' ? 'is-margin-left' : '' }}">
                    <h5 class="title is-5 is-size-6-mobile is-family-primary">
                        {{$title}}
                    </h5>
                    <ul class="is-size-7">
                        @foreach($columns as $column)
                            <li><a href="{{ $column['link'] }}">{{ $column['title'] }}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endforeach
            <div class="column is-3 is-hidden-mobile"></div>
        </div>
        <div class="footer-flex">
            <div class="footer-social">
                <a href="https://www.facebook.com/LuxehotelsNL" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 61.833 121.667"
                         enable-background="new 0 0 61.833 121.667" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <path d="M41.5 121.667h-25v-57.5H0V41.833h15.167c0 0 0-9.333 0-14.5S18.167 0 47.333 0C56 0 61.5 1.5 61.5 1.5l-0.333 19.167c0 0-19.833-1.833-19.833 11.5 0 9.167 0 9.667 0 9.667h20.5l-2.5 21.843H41.5V121.667z"></path>
                    </svg>
                </a>
                <a href="https://www.instagram.com/luxehotelsnederland" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1"
                         id="Logo" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                         xml:space="preserve">
                            <path d="M256,49.5c67.3,0,75.2,0.3,101.8,1.5c24.6,1.1,37.9,5.2,46.8,8.7c11.8,4.6,20.2,10,29,18.8c8.8,8.8,14.3,17.2,18.8,29  c3.4,8.9,7.6,22.2,8.7,46.8c1.2,26.6,1.5,34.5,1.5,101.8s-0.3,75.2-1.5,101.8c-1.1,24.6-5.2,37.9-8.7,46.8  c-4.6,11.8-10,20.2-18.8,29c-8.8,8.8-17.2,14.3-29,18.8c-8.9,3.4-22.2,7.6-46.8,8.7c-26.6,1.2-34.5,1.5-101.8,1.5  s-75.2-0.3-101.8-1.5c-24.6-1.1-37.9-5.2-46.8-8.7c-11.8-4.6-20.2-10-29-18.8c-8.8-8.8-14.3-17.2-18.8-29  c-3.4-8.9-7.6-22.2-8.7-46.8c-1.2-26.6-1.5-34.5-1.5-101.8s0.3-75.2,1.5-101.8c1.1-24.6,5.2-37.9,8.7-46.8c4.6-11.8,10-20.2,18.8-29  c8.8-8.8,17.2-14.3,29-18.8c8.9-3.4,22.2-7.6,46.8-8.7C180.8,49.7,188.7,49.5,256,49.5 M256,4.1c-68.4,0-77,0.3-103.9,1.5  C125.3,6.8,107,11.1,91,17.3c-16.6,6.4-30.6,15.1-44.6,29.1c-14,14-22.6,28.1-29.1,44.6c-6.2,16-10.5,34.3-11.7,61.2  C4.4,179,4.1,187.6,4.1,256c0,68.4,0.3,77,1.5,103.9c1.2,26.8,5.5,45.1,11.7,61.2c6.4,16.6,15.1,30.6,29.1,44.6  c14,14,28.1,22.6,44.6,29.1c16,6.2,34.3,10.5,61.2,11.7c26.9,1.2,35.4,1.5,103.9,1.5s77-0.3,103.9-1.5c26.8-1.2,45.1-5.5,61.2-11.7  c16.6-6.4,30.6-15.1,44.6-29.1c14-14,22.6-28.1,29.1-44.6c6.2-16,10.5-34.3,11.7-61.2c1.2-26.9,1.5-35.4,1.5-103.9  s-0.3-77-1.5-103.9c-1.2-26.8-5.5-45.1-11.7-61.2c-6.4-16.6-15.1-30.6-29.1-44.6c-14-14-28.1-22.6-44.6-29.1  c-16-6.2-34.3-10.5-61.2-11.7C333,4.4,324.4,4.1,256,4.1L256,4.1z"></path>
                        <path d="M256,126.6c-71.4,0-129.4,57.9-129.4,129.4S184.6,385.4,256,385.4S385.4,327.4,385.4,256S327.4,126.6,256,126.6z M256,340  c-46.4,0-84-37.6-84-84s37.6-84,84-84c46.4,0,84,37.6,84,84S302.4,340,256,340z"></path>
                        <circle cx="390.5" cy="121.5" r="30.2"></circle>
                        </svg>
                </a>
                <a href="#">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 103.15 83.829"
                         enable-background="new 0 0 103.15 83.829"
                         class="injected-svg inject-svg a-icon as--30 as--white"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                        <path d="M103.15 9.923c-3.795 1.683-7.873 2.821-12.154 3.333 4.369-2.619 7.726-6.766 9.306-11.708 -4.091 2.425-8.618 4.186-13.438 5.135C83.002 2.57 77.502 0 71.416 0 57.757 0 47.714 12.748 50.802 25.985 33.213 25.103 17.619 16.677 7.183 3.874c-5.546 9.509-2.876 21.955 6.549 28.254 -3.47-0.109-6.733-1.062-9.586-2.647 -0.229 9.805 6.798 18.973 16.974 21.018 -2.979 0.81-6.241 0.996-9.557 0.362 2.692 8.408 10.508 14.525 19.77 14.696C22.434 72.53 11.229 75.644 0 74.32c9.365 6.004 20.49 9.509 32.441 9.509 39.289 0 61.488-33.187 60.15-62.951C96.727 17.895 100.314 14.167 103.15 9.923z"></path>
                    </svg>
                </a>
                <a href="https://nl.pinterest.com/luxehotelsNL" target="_blank">
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="96.526 50 318.949 412.001"
                         enable-background="new 96.526 50 318.949 412.001"
                         class="injected-svg inject-svg a-icon as--30 as--white"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                        <path d="M265.653 50C153.22 50 96.526 130.612 96.526 197.833c0 40.702 15.411 76.914 48.462 90.41 5.418 2.215 10.275 0.077 11.847-5.925 1.093-4.152 3.681-14.631 4.835-18.99 1.584-5.936 0.97-8.018-3.403-13.191 -9.53-11.242-15.621-25.795-15.621-46.408 0-59.806 44.746-113.343 116.516-113.343 63.549 0 98.465 38.831 98.465 90.69 0 68.234-30.198 125.824-75.026 125.824 -24.759 0-43.29-20.476-37.351-45.586 7.114-29.98 20.894-62.333 20.894-83.975 0-19.37-10.397-35.527-31.918-35.527 -25.308 0-45.639 26.182-45.639 61.254 0 22.337 7.549 37.446 7.549 37.446s-25.898 109.737-30.439 128.954c-9.043 38.273-1.36 85.193-0.708 89.932 0.381 2.809 3.989 3.476 5.621 1.354 2.335-3.047 32.479-40.26 42.727-77.441 2.899-10.527 16.647-65.049 16.647-65.049 8.222 15.684 32.251 29.497 57.806 29.497 76.074 0 127.688-69.354 127.688-162.187C415.474 115.374 356.018 50 265.653 50z"></path>
                    </svg>
                </a>
            </div>
            <div class="copyright is-size-7">
                <span>Copyright (c) 2019-{{ now()->year }} <a href="{{ url()->to('/') }}">Luxe-Hotels.nu</a></span>
            </div>
        </div>
    </div>
</footer>
</body>
</html>